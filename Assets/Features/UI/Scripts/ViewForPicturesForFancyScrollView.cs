using System.Collections.Generic;
using System.IO;
using System.Linq;
using Gamesture.UI;
using TMPro;
using UnityEngine;

namespace Gamesture.Views
{
    public class ViewForPicturesForFancyScrollView : MonoBehaviour
    {
        [SerializeField] private PicturesFancyScrollView _scrollView;
        [SerializeField] private TMP_Text _labelForMissingImages;

        private const string FilesExtension = "*.png";
        private const string FilesSubfolder = "FancyScrollView";

        private Dictionary<string, Picture> _dictPictures;

        private void Awake() =>
            _dictPictures = new Dictionary<string, Picture>();

        private void Start() =>
            Load();

        public void Load()
        {
            var streamingAssetsPath = Path.Combine(Application.streamingAssetsPath, FilesSubfolder);
            Directory.CreateDirectory(streamingAssetsPath);

            var validFiles = Directory.GetFiles(streamingAssetsPath, FilesExtension);

            var dictUpdate = new Dictionary<string, Picture>();
            foreach (var path in validFiles)
            {
                if (_dictPictures.ContainsKey(path))
                {
                    dictUpdate.Add(path, _dictPictures[path]);
                    _dictPictures.Remove(path);
                }
                else
                {
                    dictUpdate.Add(path, new Picture(path));
                }
            }

            _dictPictures.Clear();;
            _dictPictures = dictUpdate;

            _labelForMissingImages.text = $"<color=yellow>No images found at</color><br>{streamingAssetsPath}";
            _labelForMissingImages.gameObject.SetActive(!_dictPictures.Any());

            _scrollView.SetContent(_dictPictures.Values.ToList());
        }
    }
}