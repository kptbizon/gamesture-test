using System;
using System.IO;
using Gamesture.Textures;
using Gamesture.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI.Extensions;

namespace Gamesture.Views
{
    public class ViewForPictureForFancyScroll : FancyCell<Picture, ScrollViewSelectContext>
    {
        [SerializeField] private TextureLoader _loaderForPicture;
        [SerializeField] private TMP_Text _labelForPath;
        [SerializeField] private TMP_Text _labelForTime;
        [SerializeField] private Canvas _canvas;

        public static readonly int ScrollHash = Animator.StringToHash("Scroll");

        private Animator _animator;
        private Picture _content;

        private const int MaxSortingOrder = 100;

        private void Reset() =>
            _animator = GetComponent<Animator>();

        private void Awake() =>
            _animator ??= GetComponent<Animator>();

        private void Update() =>
            UpdateTimeSinceCreate();

        private void OnDestroy() =>
            Clean();

        public override void UpdateContent(Picture content)
        {
            if (_content == content)
                return;

            Clean();

            _content = content;
            _labelForPath.text = Path.GetFileNameWithoutExtension(_content.Path);
            _loaderForPicture.Load(_content.Path);
        }

        public override void UpdatePosition(float position)
        {
            _canvas.sortingOrder = -(MaxSortingOrder - Mathf.RoundToInt((position < 0.5f ? position : 1 - position) * MaxSortingOrder));

            _animator.Play(ScrollHash, -1, position);
            _animator.speed = 0f;
        }

        public void Clean()
        {
            if (_content == null)
                return;

            _loaderForPicture.UnLoad(_content.Path);
        }

        private void UpdateTimeSinceCreate() =>
            _labelForTime.text = $"Created {(long)(DateTime.Now - _content.CreateTime).TotalSeconds}s ago";
    }
}