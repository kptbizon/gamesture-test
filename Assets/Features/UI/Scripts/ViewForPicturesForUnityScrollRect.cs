using System.Collections.Generic;
using System.IO;
using System.Linq;
using Gamesture.Pooling;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Gamesture.Views
{
    public class ViewForPicturesForUnityScrollRect : MonoBehaviour
    {
        [SerializeField] private PoolForPictureView _poolForViews;
        [SerializeField] private ScrollRect _scrollRect;
        [SerializeField] private TMP_Text _labelForMissingImages;

        [SerializeField] private HorizontalOrVerticalLayoutGroup _layoutGroup;

        private const string FilesExtension = "*.png";
        private const string FilesSubfolder = "ScrollRect";
        private const int InvisibleViewsToLoadCount = 2;

        private Dictionary<string, ViewForPictureForUnityScroll> _dictPictures;
        private List<ViewForPictureForUnityScroll> _views;

        private int _firstVisibleIndex = -1;
        private int _lastVisibleIndex = -1;

        private void Awake() =>
            _dictPictures = new Dictionary<string, ViewForPictureForUnityScroll>();

        private void Start()
        {
            Load();
            _scrollRect.onValueChanged.AddListener(RefreshViews);
        }


        private void OnDestroy()
        {
            _scrollRect.onValueChanged.RemoveListener(RefreshViews);
        }

        public void Load()
        {
            var streamingAssetsPath = Path.Combine(Application.streamingAssetsPath, FilesSubfolder);
            Directory.CreateDirectory(streamingAssetsPath);

            var validFiles = Directory.GetFiles(streamingAssetsPath, FilesExtension);

            // creating updated files list
            var dictUpdate = new Dictionary<string, ViewForPictureForUnityScroll>();
            foreach (var path in validFiles)
            {
                if (_dictPictures.ContainsKey(path))
                {
                    dictUpdate.Add(path, _dictPictures[path]);
                    _dictPictures.Remove(path);
                }
                else
                {
                    dictUpdate.Add(path, CreateView(path));
                }
            }

            // removing not existing files
            foreach (var path in _dictPictures.Keys)
                DestroyView(path);

            _dictPictures.Clear();;
            _dictPictures = dictUpdate;


            _labelForMissingImages.text = $"<color=yellow>No images found at</color><br>{streamingAssetsPath}";
            _labelForMissingImages.gameObject.SetActive(!_dictPictures.Any());

            _views = _dictPictures.Values.ToList();
            RefreshAllViews();
        }

        public void Clean()
        {
            foreach (var view in _dictPictures.Values)
                Destroy(view.gameObject);

            _dictPictures.Clear();
        }

        private ViewForPictureForUnityScroll CreateView(string path)
        {
            var view = _poolForViews.Acquire(_scrollRect.content);
            view.Content = new Picture(path);
            view.gameObject.SetActive(true);
            return view;
        }

        private void DestroyView(string path)
        {
            var view = _dictPictures[path];
            view.Content = null;
            _poolForViews.Release(view);
            view.gameObject.SetActive(false);
        }

        private void RefreshAllViews()
        {
            (_firstVisibleIndex, _lastVisibleIndex) = CalculateVisibleRange();

            for (var i = 0; i < _views.Count; ++i)
                _views[i].IsVisible.Value = i >= _firstVisibleIndex && i <= _lastVisibleIndex;
        }

        private void RefreshViews(Vector2 _) =>
            RefreshVisibleViews();

        private void RefreshVisibleViews()
        {
            var (firstIndex, lastIndex) = CalculateVisibleRange();

            var lowerMin = Mathf.Min(firstIndex, _firstVisibleIndex);
            var lowerMax = Mathf.Max(firstIndex, _firstVisibleIndex);

            var upperMin = Mathf.Min(lastIndex, _lastVisibleIndex);
            var upperMax = Mathf.Max(lastIndex, _lastVisibleIndex);

            for (var i = lowerMin; i < lowerMax; ++i)
                _views[i].IsVisible.Value = !_views[i].IsVisible.Value;


            for (var i = upperMin+1; i <= upperMax; ++i)
                _views[i].IsVisible.Value = !_views[i].IsVisible.Value;

            _firstVisibleIndex = firstIndex;
            _lastVisibleIndex = lastIndex;
        }

        private (int firstIndex, int lastIndex) CalculateVisibleRange()
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(_scrollRect.content);

            var view = _views.FirstOrDefault();
            if (view == null)
                return (0, 0);

            var viewRectTransform = view.RectTransform;

            var content = _scrollRect.content;
            var viewport = _scrollRect.viewport;

            var position = content.anchoredPosition;
            var viewSize = viewRectTransform.rect.size;

            var firstIndex = Mathf.Max(Mathf.FloorToInt((position.y - _layoutGroup.padding.top) / (viewSize.y + _layoutGroup.spacing)) - InvisibleViewsToLoadCount, 0);
            var lastIndex = Mathf.Min(Mathf.CeilToInt(firstIndex + viewport.rect.height / (viewSize.y + _layoutGroup.spacing)) + InvisibleViewsToLoadCount, _views.Count - 1);

            return (firstIndex, lastIndex);
        }
    }
}