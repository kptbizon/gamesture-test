using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;

namespace Gamesture.UI
{
    public class ScrollViewSelectContext
    {
        public int SelectedIndex = -1;
    }

    public class PicturesFancyScrollView : FancyScrollView<Picture, ScrollViewSelectContext>
    {
        [SerializeField] protected GameObject _selectPrefab;
        [SerializeField] protected Scroller _scroller;

        public void SetContent(List<Picture> data)
        {
            UpdateContents(data);
            _scroller.OnValueChanged(UpdatePosition);
            _scroller.OnSelectionChanged(UpdateSelection);
            _scroller.SetTotalCount(data.Count);
        }

        private void UpdateSelection(int index)
        {
            Context.SelectedIndex = index;
            Refresh();
        }

        protected override GameObject CellPrefab => _selectPrefab;
    }
}