using System;

namespace Gamesture
{
    public class Picture
    {
        private string _path;
        private DateTime _createTime;

        public Picture(string path)
        {
            _path = path;
            _createTime = DateTime.Now;
        }

        public string Path => _path;
        public DateTime CreateTime => _createTime;
    }
}