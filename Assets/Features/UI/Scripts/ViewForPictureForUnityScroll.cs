using System;
using System.IO;
using Gamesture.Textures;
using TMPro;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace Gamesture.Views
{
    public class ViewForPictureForUnityScroll : MonoBehaviour
    {
        [SerializeField] private TextureLoader _loaderForPicture;
        [SerializeField] private TMP_Text _labelForPath;
        [SerializeField] private TMP_Text _labelForTime;

        private Picture _content;
        private IDisposable _timeObserver;

        private BoolReactiveProperty _isVisible;
        private RectTransform _rectTransform;

        private void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
            _isVisible = new BoolReactiveProperty(false);
            enabled = false;
        }

        private void OnDestroy()
        {
            _content = null;
        }

        public Picture Content
        {
            get => _content;
            set => SetContent(value);
        }

        private void SetContent(Picture content)
        {
            if (_content == content)
                return;

            Clean();

            _content = content;
            if (_content == null)
                return;

            _labelForPath.text = Path.GetFileNameWithoutExtension(_content.Path);

            _isVisible
                .Subscribe(v => enabled = v)
                .AddTo(this);
        }

        private void OnEnable()
        {
            _timeObserver = this.UpdateAsObservable()
                .Subscribe(_ => UpdateTimeSinceCreate())
                .AddTo(this);

            _loaderForPicture.Load(_content.Path);
        }

        private void OnDisable()
        {
            Clean();
        }

        public void Clean()
        {
            if (_content == null)
                return;

            _timeObserver?.Dispose();
            _timeObserver = null;

            _loaderForPicture.UnLoad(_content.Path);
        }

        private void UpdateTimeSinceCreate() =>
            _labelForTime.text = $"Created {(long)(DateTime.Now - _content.CreateTime).TotalSeconds}s ago";

        public RectTransform RectTransform => _rectTransform;

        public BoolReactiveProperty IsVisible => _isVisible;
    }
}