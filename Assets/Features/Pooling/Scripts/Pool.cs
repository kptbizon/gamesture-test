using System.Collections.Generic;
using UnityEngine;

namespace Gamesture.Pooling
{
    public abstract class Pool<T> : MonoBehaviour where T : MonoBehaviour
    {
        [SerializeField] private T _prefab;

        private readonly Queue<T> _pool = new Queue<T>();
        private Transform _transform;

        protected virtual void Awake() =>
            _transform = transform;

        protected virtual void OnDestroy() =>
            _pool.Clear();

        public void Release(T element)
        {
            element.transform.SetParent(_transform);

            _pool.Enqueue(element);
        }

        public T Acquire(Transform transformForElement)
        {
            if (0 == _pool.Count)
                _pool.Enqueue(Instantiate(_prefab));

            var element = _pool.Dequeue();
            element.transform.SetParent(transformForElement);
            return element;
        }
    }
}