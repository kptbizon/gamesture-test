using System.Collections.Generic;
using Gamesture.Textures;
using UnityEngine;

namespace Gamesture.Managers
{
    public class GameManager : Manager
    {
        [SerializeField] private List<Manager> _managersToSpawn;

        public static TexturesManager TexturesMgr;

        private void Awake()
        {
            foreach (var manager in _managersToSpawn)
                Instantiate(manager, transform);
        }
    }
}