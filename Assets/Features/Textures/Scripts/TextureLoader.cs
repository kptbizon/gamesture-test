using System;
using Gamesture.Managers;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Gamesture.Textures
{
    [RequireComponent(typeof(RawImage), typeof(AspectRatioFitter))]
    public class TextureLoader : MonoBehaviour
    {
        private RawImage _rawImage;
        private AspectRatioFitter _aspectRatioFitter;
        private IDisposable _textureObserver;

        private RectTransform _rectTransform;
        private Rect _imageRect;

        private void Awake()
        {
            _rawImage = GetComponent<RawImage>();
            _aspectRatioFitter = GetComponent<AspectRatioFitter>();
            _rectTransform = GetComponent<RectTransform>();
        }

        private void Start() =>
            _imageRect = _rectTransform.rect;

        public void Load(string path)
        {
            _textureObserver = GameManager.TexturesMgr.Load(path)
                .Subscribe(OnLoad)
                .AddTo(this);
        }

        public void UnLoad(string path)
        {
            _textureObserver?.Dispose();
            _textureObserver = null;

            GameManager.TexturesMgr.UnLoad(path);
        }

        private void OnLoad(Texture texture)
        {
            _rawImage.texture = texture;
            _rawImage.enabled = _rawImage.texture != null;

            PreserveAspectRatio();
        }


        private void PreserveAspectRatio()
        {
            var texture = _rawImage.texture;
            if (texture == null)
                return;

            _rectTransform.sizeDelta = new Vector2(texture.width, texture.height);
            _aspectRatioFitter.aspectRatio = texture.width / (float)texture.height;
        }
    }
}