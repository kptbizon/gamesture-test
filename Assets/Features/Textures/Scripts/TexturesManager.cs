using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using Gamesture.Managers;
using UniRx;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Networking;

namespace Gamesture.Textures
{
    public class TexturesManager : Manager
    {
        [SerializeField, Range(0f, 5f)] private float _unloadWhenUnusedAfterInSeconds = 2f;
        private class TextureRequestData
        {
            public string Path;
            public readonly ReactiveProperty<Texture> Texture = new ReactiveProperty<Texture>(null);
            public int UseCount = 1;
            public bool WaitingForDispose = false;
            public DateTime DisposeTime;
            public bool IsDisposed = false;
        }

        private Dictionary<string, TextureRequestData> _texInfoDict;
        private List<TextureRequestData> _awaitingDisposal;

        private Queue<TextureRequestData> _waitingForLoadQueue;

        private bool _isLoading;

        private void Awake()
        {
            GameManager.TexturesMgr = this;

            _texInfoDict = new Dictionary<string, TextureRequestData>();
            _awaitingDisposal = new List<TextureRequestData>();

            _waitingForLoadQueue = new Queue<TextureRequestData>();
        }

        private void Update()
        {
            if (_awaitingDisposal.Count != 0)
            {
                var now = DateTime.Now;
                var data = _awaitingDisposal[0];
                if (!data.WaitingForDispose || now >= data.DisposeTime)
                {
                    _awaitingDisposal.RemoveAt(0);
                    if (data.WaitingForDispose)
                        Dispose(data);
                }
            }
        }

        public ReactiveProperty<Texture> Load(string path)
        {
            Assert.IsFalse(string.IsNullOrEmpty(path));

            TextureRequestData data;
            if (!_texInfoDict.ContainsKey(path))
            {
                data = new TextureRequestData { Path = path};
                _texInfoDict.Add(path, data);

                _waitingForLoadQueue.Enqueue(data);

                //TryToLoadByCoroutine();
                TryToLoadByUniAsync();
            }
            else
            {
                data = _texInfoDict[path];
                ++data.UseCount;
                CancelDisposeSchedule(data);
            }

            return data.Texture;
        }

        private void TryToLoadByCoroutine()
        {
            if (_isLoading)
                return;

            while (_waitingForLoadQueue.Count != 0)
            {
                var data = _waitingForLoadQueue.Dequeue();
                if (!data.IsDisposed)
                {
                    _isLoading = true;
                    StartCoroutine(LoadCoroutine(data));
                    return;
                }
            }
        }

        private IEnumerator LoadCoroutine(TextureRequestData data)
        {
            using var request = UnityWebRequestTexture.GetTexture(data.Path);

            yield return request.SendWebRequest();
            if (UnityWebRequest.Result.ConnectionError == request.result ||
                UnityWebRequest.Result.ProtocolError == request.result)
            {
                Debug.LogError($"<color=red>Unable to load image at <color=yellow>{request.url}</color> with error <color=white>{request.error}</color></color>", this);
            }
            else
            {
                data.Texture.Value = DownloadHandlerTexture.GetContent(request);
                request.Dispose();

                if (data.IsDisposed)
                    Dispose(data);
            }

            _isLoading = false;
            TryToLoadByCoroutine();
        }

        private async void TryToLoadByUniAsync()
        {
            if (_isLoading)
                return;

            while (_waitingForLoadQueue.Count != 0)
            {
                var data = _waitingForLoadQueue.Dequeue();
                if (!data.IsDisposed)
                {
                    _isLoading = true;
                    data.Texture.Value = await LoadUniAsync(data.Path);
                    if (data.IsDisposed)
                        Dispose(data);
                    _isLoading = false;
                }
            }
        }

        private async UniTask<Texture> LoadUniAsync(string path)
        {
            try
            {
                var request = await UnityWebRequestTexture.GetTexture(path).SendWebRequest();
                return DownloadHandlerTexture.GetContent(request);
            }
            catch (Exception e)
            {
                Debug.LogError($"<color=red>Unable to load image at <color=yellow>{path}</color> with error <color=white>{e.Message}</color></color>", this);
                return null;
            }
        }

        public void UnLoad(string path)
        {
            Assert.IsFalse(string.IsNullOrEmpty(path));

            if (_texInfoDict.ContainsKey(path))
            {
                var data = _texInfoDict[path];
                --data.UseCount;

                if (data.UseCount <= 0)
                    ScheduleForDispose(data);
            }
        }

        private void ScheduleForDispose(TextureRequestData data)
        {
            if (data.WaitingForDispose)
                return;

            data.WaitingForDispose = true;
            data.DisposeTime = DateTime.Now + TimeSpan.FromSeconds(_unloadWhenUnusedAfterInSeconds);
            _awaitingDisposal.Add(data);
        }

        private void CancelDisposeSchedule(TextureRequestData data)
        {
            if (!data.WaitingForDispose)
                return;

            data.WaitingForDispose = false;
        }

        private void Dispose(TextureRequestData data)
        {
            _texInfoDict.Remove(data.Path);
            Destroy(data.Texture.Value);
            data.Texture.Value = null;
            data.Texture.Dispose();
            data.IsDisposed = true;
        }
    }
}